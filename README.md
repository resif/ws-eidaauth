# Method /auth for EIDA authentication

This projects is the implementation of the `/auth` method as described in EIDA.

Input : a signed token (validity will be checked by the program)

Output : a login and password in the `login:password` form

This login and password is valid for a certain amount of time (24h typically)

## Configuration

To configure the database connection, copy `configurations/default.py` to `configurations/production.py` for instance. Then edit the file with everything you need.

From the value of the `RUNMODE` environment variable, the name of the configuration file is choosen.

``` shell
RUNMODE=development python eidawsauth.py
```

## Database initialisation

### User and minimum privileges

``` sql
grant connect on database "resifAuth" to eidawsauth;
grant connect on database "resifInv-Prod" to eidawsauth;
\c "resifAuth"
grant select,insert,update,delete on table users,credentials TO eidawsauth ;
grant select,update on sequence  users_user_index_seq TO eidawsauth ;
\c "resifInv-Prod"
grant select,insert,update,delete on table eida_temp_users TO eidawsauth;
grant select on table networks to eidawsauth;
grant select on table resif_users to eidawsauth;
grant select,update on sequence aut_user_user_id_seq to eidawsauth ;
```

### Expected tables schema

#### AUTHDB
Table `users`:
From the existing table, we have to add an `expires_at` column.

``` sql
alter table users add column if not exists expires_at timestamp default value null;
```

Table `credentials` :
``` sql
alter table credentials add column if not exists expires_at timestamp default value null;
```


#### PRIVILEDGEDB
Table `eida_temp_users` :
``` sql
alter table aut_user add column if not exists expires_at timestamp default value null;


CREATE TABLE public.eida_temp_users (
    network text,
    start_year integer,
    end_year integer,
    name text,
    network_id bigint DEFAULT 0,
    expires_at timestamp without time zone
);

ALTER TABLE ONLY public.eida_temp_users
    ADD CONSTRAINT unique_privilege UNIQUE (name, network_id);

ALTER TABLE ONLY public.eida_temp_users
    ADD CONSTRAINT eida_temp_users_network_id_fkey FOREIGN KEY (network_id) REFERENCES public.networks(network_id) ON DELETE SET DEFAULT;


GRANT ALL ON TABLE public.eida_temp_users TO eidawsauth;
GRANT SELECT ON TABLE public.eida_temp_users TO resifinv_ro;
```

Table `epos_network_map`

``` sql
create table epos_network_map (
    epos_network_map_id bigint generated always as identity,
    epos_name text, network_id bigint,
    created_at timestamp with time zone default now(),
    updated_at timestamp with time zone default now(),
    primary key(epos_network_map_id),
    constraint fk_network foreign  key(network_id) references networks(network_id) );
grant select on epos_network_map to eidawsauth;
insert into epos_network_map (epos_name, network_id) values ('/epos/alparray', 34);
```

## Playing around

After the Database initialisation, the application can be run in a virtual environment.

``` shell
pip install -r requirements.txt
cd eidawsauth
FLASK_ENV=development RUNMODE=development python eidawsauth.py
```
Then, to send a post request :

``` shell
http localhost:8000/version
http POST localhost:8000 < token.asc
```

## Running tests

## Configuration

The configuration is made through environment variables.

### Databases access
For convenience, all the database connection parameters (except password) are hardcoded when `RUNMODE` is set to `production` or `preproduction`

Look at `eidawsauth/config.py` to see the list en environments needed. Tere are sensitive defaults for RESIF.

Minimal environment for RESIF is 

  * `RUNMODE`: set to `production` or `preproduction`
  * `PGPASSWORD`: if same user is set for both databases, this value will be used for both. Otherwise, set `RESIFINV_PGPASSWORD` and `RESIFAUTH_PGPASSWORD`
  
  

# Explanations

What does this program do ?

## Steps

0. Get all configurations and setup database connections
1. Read the data from POST request
   NOTE : We should put a size limit on the WSGI server
2. Verify the token's signature using the geofon public key
2. Parse the token's informations
3. Compute a random login and password
4. Register this in the resifAuth database, along with the `expires_at` value (24h)
5. From the `member-of` field in the token :
   - do the mapping from EPOS names to FDSN reference from the epos_fdsn table in the resifAuth database
     the FDSN reference is the network name, startyear, endyear
   - register the login along with the FDSN references and the expiration date in the resifInv-Prod database, table `access`
6. Return the `login:password` to the client

# Other methods

## /version

returns the version number and environment string.

## /cleanup

Remove old users, credentials and privileges.

It's probably a good idea to protect this method at the webserver level.
