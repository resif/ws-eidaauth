FROM python:3.10-slim AS build
RUN apt-get update && apt-get install -y libpq-dev build-essential
COPY requirements.txt /
RUN pip3 install --no-cache-dir -r /requirements.txt

FROM python:3.10-slim
RUN apt-get update && apt-get -y install gnupg libpq5
COPY setup/gpghome /gpghome
COPY --from=build /usr/local/lib/python3.10/site-packages/ /usr/local/lib/python3.10/site-packages
RUN pip3 install --no-cache-dir gunicorn
COPY eidawsauth/ /app/
WORKDIR /app
USER 1000
CMD ["gunicorn", "-b", "0.0.0.0:8000", "eidawsauth"]
