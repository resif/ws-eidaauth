import os
import json


class Configurator:
    """
    All the configuration logic for this app goes here.
    """

    # For convenience, prepare configuration for RESIF RUNMODE
    if os.getenv("RUNMODE") == "production":
        ENVIRONMENT = "production"
        RESIFINV_PGUSER = "eidawsauth"
        RESIFINV_PGHOST = "resif-pgprod.u-ga.fr"
        RESIFINV_PGPORT = 5432
        RESIFINV_PGDATABASE = "resifInv-Prod"
        RESIFAUTH_PGUSER = "eidawsauth"
        RESIFAUTH_PGHOST = "resif-pgprod.u-ga.fr"
        RESIFAUTH_PGPORT = 5432
        RESIFAUTH_PGDATABASE = "resifAuth"
    elif os.getenv("RUNMODE") == "preprod":
        ENVIRONMENT = "preprod"
        RESIFINV_PGUSER = "eidawsauth"
        RESIFINV_PGHOST = "resif-pgpreprod.u-ga.fr"
        RESIFINV_PGPORT = 5432
        RESIFINV_PGDATABASE = "resifInv-Preprod"
        RESIFAUTH_PGUSER = "eidawsauth"
        RESIFAUTH_PGHOST = "resif-pgpreprod.u-ga.fr"
        RESIFAUTH_PGPORT = 5432
        RESIFAUTH_PGDATABASE = "resifAuth"
    else:
        ENVIRONMENT = "custom"
        RESIFINV_PGUSER = os.getenv("RESIFINV_PGUSER", "eidawsauth")
        RESIFINV_PGHOST = os.getenv("RESIFINV_PGHOST", "localhost")
        RESIFINV_PGPORT = os.getenv("RESIFINV_PGPORT", 5432)
        RESIFINV_PGDATABASE = os.getenv("RESIFINV_PGDATABASE", "resifInv")
        RESIFAUTH_PGUSER = os.getenv("RESIFAUTH_PGUSER", "eidawsauth")
        RESIFAUTH_PGHOST = os.getenv("RESIFAUTH_PGHOST", "localhost")
        RESIFAUTH_PGPORT = os.getenv("RESIFAUTH_PGPORT", 5432)
        RESIFAUTH_PGDATABASE = os.getenv("RESIFAUTH_PGDATABASE", "resifAuth")

    # if PGPASS is provided, then use it for both database connection
    if "PGPASSWORD" in os.environ:
        RESIFINV_PGPASSWORD = os.getenv("PGPASSWORD")
        RESIFAUTH_PGPASSWORD = os.getenv("PGPASSWORD")
    else:
        RESIFINV_PGPASSWORD = os.getenv("RESIFINV_PGPASSWORD")
        RESIFAUTH_PGPASSWORD = os.getenv("RESIFAUTH_PGPASSWORD")

    GNUPG_HOMEDIR = os.getenv("GNUPG_HOMEDIR", "/gpghome")
    SUPPORT_EMAIL = os.getenv("SUPPORT_EMAIL", "resif-dc@univ-grenoble-alpes.fr")

    if not RESIFINV_PGPASSWORD:
        raise ValueError(
            "No value for RESIFINV_PGPASSWORD, please set environment variable PGPASSWORD or RESIFINV_PGPASSWORD"
        )
    if not RESIFAUTH_PGPASSWORD:
        raise ValueError(
            "No value for RESIFAUTH_PGPASSWORD, please set environment variable PGPASSWORD or RESIFAUTH_PGPASSWORD"
        )
