#!/bin/bash

# handles EIDA authentication
# https://dev.knmi.nl/projects/eida/wiki/FDSN_-_dataselect_-_auth
#
# This is a prototype code designed to run under IRIS WSS
# https://seiscode.iris.washington.edu/projects/webserviceshell/wiki/Working_with_Handlers
# see also FIXME tags
#

# check some tools are installed
command -v gpg >/dev/null 2>&1 || { echo >&2 "Missing gpg. Check path? Aborting."; exit 1; }
command -v python >/dev/null 2>&1 || { echo >&2 "Missing python. Check path? Aborting."; exit 1; }
command -v resifauth.sh >/dev/null 2>&1 || { echo >&2 "Missing resifauth.sh. Check path? Aborting."; exit 1; }

# logging command
LOGGER="logger -s -p authpriv.notice -t eida_auth_handler"
$LOGGER "WARNING this is a prototype code. More info in code comments." 

# must be called with --STDIN (see IRIS WSS doc)
[ "$1" != "--STDIN" ] && $LOGGER "Incorrect call." && echo "Incorrect call." && exit 1

# get stdin
token=$(cat)

# ".. You will first need to download the GFZ public key which was used to issue the 
# token, from http://eida.gfz-potsdam.de/eida-gfz.asc and import it into your GPG keyring. 
# You may ignore the warning about the key being untrusted: .."
#
# FIXME will have to download other institute's public keys
wget  http://eida.gfz-potsdam.de/eida-gfz.asc -O /tmp/eida-gfz.asc &> /dev/null
gpg --import /tmp/eida-gfz.asc &> /dev/null

# decrypt token
json=`echo "$token" | gpg --decrypt 2>/dev/null`
$LOGGER json: $json

# check json format 
echo "$json" | python -m json.tool &> /dev/null
if [ $? -ne 0 ]; then
	$LOGGER "Not a valid JSON. Check your authentication token."
	exit 1
fi

# extract user's email from JSON content.
# FIXME token validity period should be checked. 
email=`echo $json | python -c "import sys, json; print json.load(sys.stdin)['mail']"`
$LOGGER email: $email

# uses email to retrieve account credentials.
$LOGGER "now launching: resifauth.sh --eidahandler $email" 
# FIXME resifauth.sh returns permanent accounts : security would
# require to return only temporary accounts (hint : such a temporary account could be 
# linked to access grants defined in its permanent account counterpart)
resifauth.sh --eidahandler $email

if [ $? -ne 0 ]; then
	$LOGGER "resifauth.sh command returned error."
	exit 2
fi

$LOGGER "exit with success." 
exit 0